ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("mapCanvas", {
            center: [55.76, 37.64],
            zoom: 10,
            controls: ['routeEditor']
        }),
        myPlacemark = new ymaps.Placemark([55.76, 37.64], {
            // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
            balloonContentHeader: "ООО «Арма»",
            balloonContentBody: "г. Москва, Дмитровское шоссе, дом 12А"
        });

    myMap.geoObjects.add(myPlacemark);
}