//Init main header slider
$('#sliderBlock').owlCarousel({
    navigation : true, // Show next and prev buttons
    navigationText : false,
    slideSpeed : 300,
    paginationSpeed : 400,
    paginationNumbers : true,
    addClassActive : true,
    singleItem:true
});
//Init map maphilight
$('.map').maphilight({
    fillColor: 'e63000',
    fillOpacity: 1
});
$('#hilightlink').mouseover(function(e) {
    $('#square2').mouseover();
}).mouseout(function(e) {
    $('#square2').mouseout();
}).click(function(e) { e.preventDefault(); });
//Choose number from list
$( ".dNumber" ).hover(
    function() {
        var number = $(this).html();
        $( '#doorsMap' ).find( 'area[title=' +number+ ']' ).mouseover();
    }, function() {
        var number = $(this).html();
        $( '#doorsMap' ).find( 'area[title=' +number+ ']' ).mouseout();
    }
);
//Choose numberfrom ImgMap
$( "#doorsMap area" ).hover(
    function() {
        var number = $(this).attr('title');
        $( '.dNumber[title=' +number+ ']' ).addClass('active');
        $( '.dNumber[title=' +number+ ']').next('span').addClass('active');
    }, function() {
        var number = $(this).attr('title');
        $( '.dNumber[title=' +number+ ']' ).removeClass('active');
        $( '.dNumber[title=' +number+ ']').next('span').removeClass('active');
    }
);
//Google map
function initialize() {
    var mapCanvas = document.getElementById('mapCanvas');
    var mapOptions = {
        center: new google.maps.LatLng(44.5403, -78.5463),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
}